const filterOutUndefined = (filters: object): object => {
  const keys = Object.keys(filters)

  return keys.reduce((acc, key) => {
    const filter = filters[key]

    if (typeof filter !== 'undefined') acc[key] = filters[key]

    return acc
  }, {})
}

export default filterOutUndefined