import {
  Image 
} from '../controllers/photos/fetchPhotos'

import {
  IAlbum 
} from '../models/Album'

const extractAlbums = (ownerId: string) => (photos: Image[]): IAlbum[] => {
  const albumIds = photos.map(photo => photo.albumId)

  const uniqueAlbumIds = [...new Set(albumIds)]

  return uniqueAlbumIds.map(id => ({
    ownerId,
    id,
    title: `${id}`
  }))
}

export default extractAlbums