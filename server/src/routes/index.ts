import express from 'express'
import asyncHandler from 'express-async-handler'

import users from '../controllers/users';
import photos from '../controllers/photos';
import albums from '../controllers/albums';

import verifyUser from '../middleware/verifyUser'

const router = express.Router();

router
  .route('/register')
  .post(
    asyncHandler(users.create),
  );

router
  .route('/login')
  .post(
    asyncHandler(users.login),
  );

/**
 * I'd rather go with 
 * router.route('/photos').get().post().delete() etc
 * as it aligns with API design best practices 
 * eg as suggested by OpenAPI docs (prefer nouns to verbs as API endpoints)
 * but it does not match the assignment description
 */

router
  .route('/load-photos')
  .post(
    verifyUser,
    asyncHandler(photos.load),
  );

router
  .route('/get-photos')
  .get(
    asyncHandler(photos.get),
  );

router
  .route('/delete-photo/:photoId')
  .delete(
    verifyUser,
    asyncHandler(photos.remove),
  );

router
  .route('/delete-album/:albumId')
  .delete(
    verifyUser,
    asyncHandler(albums.remove),
  );

router
  .route('/change-album-title/:albumId')
  .patch(
    verifyUser,
    asyncHandler(albums.changeTitle),
  );

export default router
