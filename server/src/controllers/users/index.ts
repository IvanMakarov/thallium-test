import express from 'express';

import User, {
  IUser
} from '../../models/User'

import generatePasswordHash from '../../auth/generatePasswordHash';
import comparePasswordToHash from '../../auth/comparePasswordToHash';
import buildUserJwt from '../../auth/buildUserJwt';

export default {
  create: async (req: express.Request, res: express.Response) => {
    const {
      body: {
        email,
        login,
        password
      }
    } = req

    try {
      const newUser: IUser = {
        email,
        login,
        passwordHash: generatePasswordHash(password),
        registerDate: new Date()
      }

      await User.create(newUser)

      res.status(201).send()
    } catch (error) {
      console.error(error)
      res.status(500).send()
    }
  },

  login: async (req: express.Request, res: express.Response) => {
    const {
      body: {
        email,
        login,
        password
      }
    } = req

    const user = await User.findOne({
      $or: [
        {email},
        {login}
      ]
    })

    if (!user) {
      res.status(404).send()
    }

    const isPasswordCorrect = comparePasswordToHash(password, user.passwordHash)

    if (isPasswordCorrect) {
      const token = buildUserJwt(user)
      res.status(201).send({ token })
    }
    else {
      res.status(401).send()
    }
  }
}