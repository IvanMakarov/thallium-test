import express from 'express';

import Album from '../../models/Album'

export default {
  remove: async (req: express.Request, res: express.Response) => {
    try {
      const albumId = req.params.albumId

      const ids = albumId
        .split(',')
        .map(id => +id)

      await Promise.all(ids.map(async id => {
        await Album.findOneAndRemove({ id }) 
      }))

      res.status(201).send()
    } catch (error) {
      console.error(error)
      res.status(500).send()
    }

    res.status(201).send()
  },
  changeTitle: async (req: express.Request, res: express.Response) => {
    try {
      const albumId = +req.params.albumId
      const newAlbumName = req.body.newAlbumName

      const album = await Album.findOneAndUpdate({id: albumId}, {title: newAlbumName})

      if (!album) {
        res.status(404).send()
      }
      else {
        res.status(201).send()
      }
    } catch (error) {
      res.status(500).send()
    }
  }
}