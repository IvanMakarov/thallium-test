import axios from "axios"

export interface Image {
  albumId: number,
  id: number,
  title: string,
  url: string,
  thumbnailUrl: string
}

export default async (): Promise<Image[]> => {
  const response = await axios.get(process.env.IMG_SRC)

  return response.data
}