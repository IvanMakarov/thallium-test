import express from 'express';

import Photo from '../../models/Photo'
import Album from '../../models/Album'

import filterOutUndefined from '../../utils/fiterOutUndefined';
import extractAlbums from '../../utils/extractAlbums';

import fetchPhotos from './fetchPhotos'
import processPhotos from './processPhotos'

export default {
  load: async (req: express.Request, res: express.Response) => {
    try {
      const userId = `${req.user.id}`
      const photos = await fetchPhotos()
      const processedPhotos = processPhotos(photos, userId)
  
      await Photo.insertMany(processedPhotos)
  
      const albums = extractAlbums(userId)(photos)

      await Album.insertMany(albums)

      res.status(201).send()
    } 
    catch (error) {
      res.status(500).send()
    }
  },
  get: async (req: express.Request, res: express.Response) => {
    try {
      const limit: number = +req.query.maxCount
      const skip: number = limit * +req.query.page

      const filters = {
        ownerId: req.query.ownerId
          ? `${req.query.ownerId}`
          : undefined
      }

      const photos = await Photo.find(filterOutUndefined(filters))
        .limit(limit)
        .skip(skip)

      res.status(201).send({
        photos
      })
    } catch (error) {
      res.status(500).send()
    }
  },
  remove: async (req: express.Request, res: express.Response) => {
    try {
      const photoId = req.params.photoId

      const ids = photoId
        .split(',')
        .map(id => +id)

      await Photo.deleteMany({ id: { $in: ids }})

      res.status(201).send()
    } catch (error) {
      console.error(error)
      res.status(500).send()
    }
  }
}