import {
  Image
} from './fetchPhotos'

interface OwnedPhoto extends Image {
  ownerId: string
}

export default (photos: Image[], ownerId: string): OwnedPhoto[] => photos.map(photo => ({
  ...photo,
  ownerId
}))