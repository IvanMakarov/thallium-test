import mongoose from 'mongoose'

export interface IPhoto {
  id: number,
  albumId: number,
  title: string,
  url: string,
  thumbnailUrl: string,
  ownerId: string,
}

const Photo = new mongoose.Schema<IPhoto>({
  id: { type: Number },
  albumId: { type: Number, ref: 'albums' },
  title: { type: String },
  url: { type: String },
  thumbnailUrl: { type: String },
  ownerId: { type: String, ref: 'users' },
})

export default mongoose.model('photos', Photo)