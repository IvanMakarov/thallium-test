import mongoose from 'mongoose'

import Photo from './Photo'

export interface IAlbum {
  id: number,
  title: string,
  ownerId: string,
}

const Album = new mongoose.Schema<IAlbum>({
  id: { type: Number },
  title: { type: String },
  ownerId: { type: String, ref: 'users' },
})

Album.post('findOneAndRemove', async (doc, next) => {
  if (doc) {
    await Photo.deleteMany({ albumId: doc.id })
  }

  next()
})

export default mongoose.model('albums', Album)