import mongoose from 'mongoose'
export interface IUser {
  email: string,
  login: string,
  passwordHash: string,
  registerDate: Date,
}

const User = new mongoose.Schema<IUser>({
  email: { type: String },
  login: { type: String },
  passwordHash: { type: String },
  registerDate: { type: Date },
})

export default mongoose.model('users', User)