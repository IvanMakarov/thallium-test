import generatePasswordHash from './generatePasswordHash';

const comparePasswordToHash = (password: string, hash: string): boolean => generatePasswordHash(password) === hash

export default comparePasswordToHash