import jwt from 'jsonwebtoken';

const tokenize = (payload: object, expiresIn = '7d'): string => {
  return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn });
}

export default tokenize