import jwt from 'jsonwebtoken';

interface User {
  email: string, 
  login: string, 
  id: number
}

export default (token: string): User => {
  if (jwt.verify(token, process.env.JWT_SECRET)) {
    return jwt.decode(token);  
  }
  
  return null
} 