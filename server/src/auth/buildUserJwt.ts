import tokenize from "./tokenize"

export default (user: {email: string, login: string, _id: string}): string => {
  const userData = {
    email: user.email,
    login: user.login,
    id: user._id
  }

  return tokenize(userData)
}