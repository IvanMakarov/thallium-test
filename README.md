Start project
```
$ docker-compose up
```

Access OpenAPI GUI at localhost:8080, Mongo Express at localhost:8081 and the server itself at localhost:3000.

There are known issues with Swagger and Docker connectivity; I've used [Cors Everywhere](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/) extension for Firefox as a workaround.